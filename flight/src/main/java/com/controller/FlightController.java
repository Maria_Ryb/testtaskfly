package com.controller;

import com.dto.Passenger;
import com.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/flights")
public class FlightController {

    @Autowired
    private FlightService flightService;

    @GetMapping
    public Integer getPassengerCountInFlightClass(@RequestParam Long flightId, @RequestParam String flightClassKeyName) {
        return flightService.getAvailablePlaces(flightId, flightClassKeyName);
    }
}
