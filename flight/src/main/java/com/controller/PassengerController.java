package com.controller;

import com.dto.GroupedPassengers;
import com.dto.Passenger;
import com.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/passengers")
public class PassengerController {

    @Autowired
    private PassengerService passengerService;

    @GetMapping
    public List<Passenger> getAllPassengers() {
        return passengerService.getPassengers();
    }

    @PostMapping
    public Passenger createPassenger(@RequestBody Passenger passenger) {
        return passengerService.create(passenger);
    }

    @GetMapping("/groupedPassenger")
    public GroupedPassengers getGroupedPassengers(@RequestParam(value = "flightId", required = true) Long flightId) {
        return passengerService.getGroupedPassengers(flightId);
    }

    @DeleteMapping
    public void deletePassenger(@RequestParam(value = "id", required = true) Long id) {
        passengerService.delete(id);
    }
}
