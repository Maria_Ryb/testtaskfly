package com.dto;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Passenger {

    private Long id;

    @NotNull(message = "Please enter name")
    private String name;

    @NotNull(message = "Please enter surName")
    private String surName;

    @NotNull(message = "Please enter classType")
    private String classType;

    @NotNull
    private Long flightId;
}
