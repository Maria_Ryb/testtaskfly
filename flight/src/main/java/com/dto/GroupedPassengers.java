package com.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupedPassengers {

    private List<String> economy;

    private List<String> business;

}
