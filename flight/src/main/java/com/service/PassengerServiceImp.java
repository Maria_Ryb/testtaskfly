package com.service;

import com.domain.FlightClass;
import com.domain.FlightEntity;
import com.domain.PassengerEntity;
import com.dto.GroupedPassengers;
import com.dto.Passenger;
import com.repository.FlightRepository;
import com.repository.PassengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PassengerServiceImp implements PassengerService {

    @Autowired
    private PassengerRepository passengerRepository;

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private FlightService flightService;

    @Transactional
    @Override
    public List<Passenger> getPassengers() {
        return passengerRepository
                .findAll()
                .stream()
                .map(this::entityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public GroupedPassengers getGroupedPassengers(Long flightId) {
        FlightEntity flight = flightRepository.getOne(flightId);
        List<PassengerEntity> passengers = passengerRepository.findPassengersByFlight(flight);
        List<String> economyPassengers = passengers
                .stream()
                .filter(p -> p.getClassType() == FlightClass.ECONOMY.getNumber())
                .map(p -> String.format("%s %s", p.getName(), p.getSurName()))
                .collect(Collectors.toList());
        List<String> businessPassengers = passengers
                .stream()
                .filter(p -> p.getClassType() == FlightClass.BUSINESS.getNumber())
                .map(p -> String.format("%s %s", p.getName(), p.getSurName()))
                .collect(Collectors.toList());
        return GroupedPassengers.builder()
                .business(businessPassengers)
                .economy(economyPassengers)
                .build();
    }
    @Transactional
    @Override
    public Passenger create(Passenger passenger) {
        int availablePlaces = flightService.getAvailablePlaces(passenger.getFlightId(), passenger.getClassType());
        if(availablePlaces <= 0) {
            throw new RuntimeException("Not enough places in class");
        }
        return entityToDto(passengerRepository.save(dtoToEntity(passenger)));
    }

    @Transactional
    @Override
    public void delete(Long id) {
        passengerRepository.deleteById(id);

    }

    private Passenger entityToDto(PassengerEntity passengerEntity) {
        return Passenger.builder()
                .id(passengerEntity.getId())
                .name(passengerEntity.getName())
                .surName(passengerEntity.getSurName())
                .classType(FlightClass.getByNumber(passengerEntity.getClassType()).getKeyName())
                .flightId(passengerEntity.getFlight().getFlightId())
                .build();

    }

    private PassengerEntity dtoToEntity(Passenger passenger) {
        return PassengerEntity.builder()
                .id(passenger.getId())
                .name(passenger.getName())
                .surName(passenger.getSurName())
                .flight(flightRepository.getOne(passenger.getFlightId()))
                .classType(FlightClass.getByKeyName(passenger.getClassType()).getNumber())
                .build();
    }
}
