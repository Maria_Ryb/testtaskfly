package com.service;

import com.domain.FlightClass;
import com.dto.Flight;
import com.dto.Passenger;

import java.util.List;

public interface FlightService {

    Integer getAvailablePlaces(Long flightId, String flightClassKeyName);
}
