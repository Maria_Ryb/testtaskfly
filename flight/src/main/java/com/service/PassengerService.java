package com.service;

import com.domain.FlightClass;
import com.domain.PassengerEntity;
import com.dto.GroupedPassengers;
import com.dto.Passenger;

import java.util.List;

public interface PassengerService {

    List<Passenger> getPassengers();

    Passenger create(Passenger passenger);

    void delete(Long id);

    GroupedPassengers getGroupedPassengers(Long flightId);

}
