package com.service;

import com.domain.FlightClass;
import com.domain.FlightEntity;
import com.domain.PassengerEntity;
import com.repository.FlightRepository;
import com.repository.PassengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FlightServiceImp implements FlightService {

    @Autowired
    private PassengerRepository passengerRepository;

    @Autowired
    private FlightRepository flightRepository;



    @Override
    public Integer getAvailablePlaces(Long flightId, String flightClassKeyName) {
        FlightClass flightClass = FlightClass.getByKeyName(flightClassKeyName);
        FlightEntity flight = flightRepository.getOne(flightId);
        List<PassengerEntity> passengers = passengerRepository.findPassengersByFlight(flight);
        int totalPlaces = 0;
        int usedPlaces = (int) passengers
                .stream()
                .filter(p -> p.getClassType() == flightClass.getNumber())
                .count();
        if (flightClass == FlightClass.BUSINESS) {
            totalPlaces = flight.getBusinessСount();
        } else if (flightClass == FlightClass.ECONOMY) {
            totalPlaces = flight.getEconomyCount();
        }
        return totalPlaces - usedPlaces;
    }
}
