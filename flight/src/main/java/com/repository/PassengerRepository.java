package com.repository;

import com.domain.FlightEntity;
import com.domain.PassengerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PassengerRepository extends JpaRepository<PassengerEntity, Long> {
    List<PassengerEntity> findPassengersByFlight(FlightEntity flightEntity);
}
