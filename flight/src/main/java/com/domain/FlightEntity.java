package com.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "FLIGHTS")
public class FlightEntity {

    @Id
    @Column(name = "FLIGHT_ID")
    private Long flightId;

    @Column(name = "ECONOMY_COUNT")
    private Integer economyCount;

    @Column(name = "BUSINESS_COUNT")
    private Integer businessСount;



}
