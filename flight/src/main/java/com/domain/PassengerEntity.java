package com.domain;

import com.dto.Flight;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "PASSENGERS")
public class PassengerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SUR_NAME")
    private String surName;

    @Column(name = "CLASS_TYPE")
    private Integer classType;

    @ManyToOne
    @JoinColumn(name = "FLIGHT_ID", nullable = false)
    private FlightEntity flight;
}
