package com.domain;

public enum FlightClass {
    ECONOMY(0, "economy"),
    BUSINESS(1, "business");

    private int number;
    private String keyName;

    FlightClass(int number, String keyName) {
        this.number = number;
        this.keyName = keyName;
    }

    public int getNumber() {
        return number;
    }

    public String getKeyName() {
        return keyName;
    }

    public static FlightClass getByNumber(int number)
    {
        switch (number)
        {
            case 0: return ECONOMY;
            case 1: return BUSINESS;
            default: throw new IllegalArgumentException(String.format("Can't find FlightClass with number %d", number));
        }
    }

    public static FlightClass getByKeyName(String keyName)
    {
        if(keyName == null)
            throw new NullPointerException("keyName can not be null");
        switch (keyName.toLowerCase())
        {
            case "economy": return ECONOMY;
            case "business": return BUSINESS;
            default: throw new IllegalArgumentException(String.format("Can't find FlightClass with keyName %d", keyName));
        }
    }
}
