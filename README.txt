Запросы
       Для получения списка всех пассажиров, параметры запроса - нет, вывод - список всех пассажиров.
       GET localhost:8080/passengers

       Для регистрации нового пассажира, параметры запроса - нет, вывод - созданный пассажир.
       При успешной регистрации код - 200, при неуспешной - 500.
       POST localhost:8080/passengers
       {
           "name": "Ivan",
           "surName": "Petrov",
           "classType": "business",
           "flightId": 1
       }

       Для получения списка всех пассажиров, сгруппированных по классу полета, параметры запроса - flightId,
       вывод - список сгруппированных пассажиров.
       GET localhost:8080/passengers/groupedPassenger?flightId=1

       Для удаления пассажира. Праметры запроса - id.
       DELETE localhost:8080/passengers?id=1

       Для получения оставшихся мест в выбранном классе, параметры - flightId, flightClassKeyName,
       вывод - число оставшихся мест.

       GET http://localhost:8080/flights?flightId=1&flightClassKeyName=economy